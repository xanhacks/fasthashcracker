from std/md5 import getMD5
from std/strutils import strip, toHex, toUpperAscii
import nimSHA2

const SHA256Len = 32
proc SHA256(d: cstring, n: culong, md: cstring = nil): cstring {.cdecl, dynlib: "libssl.so", importc.}
proc getSHA256(s: string): string =
  result = ""
  let s = SHA256(s.cstring, s.len.culong)
  for i in 0 ..< SHA256Len:
    result.add s[i].BiggestInt.toHex(2)


proc generate(algo: string) =
    var outFile = open("rockyou.txt." & algo, fmWrite)
    defer: outFile.close()

    if algo == "md5":
        for line in lines "rockyou.txt":
            outFile.writeLine(toUpperAscii(getMD5(line.strip())))
    elif algo == "sha256":
        for line in lines "rockyou.txt":
            outFile.writeLine(getSHA256(line.strip()))
    elif algo == "sha512":
        for line in lines "rockyou.txt":
            outFile.writeLine(hex(computeSHA512(line.strip())))


echo "Generating MD5 wordlist ..."
generate("md5")
echo "Generating SHA256 wordlist ..."
generate("sha256")
echo "Generating SHA512 wordlist ..."
generate("sha512")
echo "3/3 done !"