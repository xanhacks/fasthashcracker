NIMFLAGS = -d=release

SERVER_SRC = ./server.nim
SERVER_OUTFILE = ./bin/server
RAINBOW_SRC = ./rainbow.nim
RAINBOW_OUTFILE = ./bin/rainbow

default: build

build:
	nim compile $(NIMFLAGS) --out=$(SERVER_OUTFILE) $(SERVER_SRC)
	nim compile $(NIMFLAGS) --out=$(RAINBOW_OUTFILE) $(RAINBOW_SRC)

rebuild: clean build

clean:
	rm -rf ./bin
