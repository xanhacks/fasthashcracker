FROM nimlang/nim:latest

WORKDIR /usr/srv/app/

RUN wget -q https://gitlab.com/kalilinux/packages/wordlists/-/raw/kali/master/rockyou.txt.gz && \
    gunzip rockyou.txt.gz

RUN apt-get update && \
    apt-get install -y --no-install-recommends make libssl-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN nimble install --accept jester nimSHA2

COPY ./Makefile .
COPY ./index.html .
COPY ./server.nim .
COPY ./rainbow.nim .

RUN make && ./bin/rainbow

EXPOSE 5000
CMD ["./bin/server"]