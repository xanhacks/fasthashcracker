# Fast HashCracker

Hash crack online using `rockyou` wordlist (rainbow table).

## Demo

- ~[https://cracker.xanhacks.xyz](https://cracker.xanhacks.xyz)~

## Features

**Wordlist :** rockyou

**Algorithm :**

- MD5
- SHA256
- SHA512

## Installation

```bash
$ git clone https://gitlab.com/xanhacks/fasthashcracker
$ cd fasthashcracker
$ docker-compose up -d
```

### Requirements

All wordlists are load in-memory to speed up the cracking time.

```bash
root@f6543fcd6090:/usr/srv/app# du -sh rock*
134M    rockyou.txt
452M    rockyou.txt.md5
890M    rockyou.txt.sha256
1.8G    rockyou.txt.sha512
```

Be sure you have enough RAM to run it.

## Made with

- Language : [Nim](https://nim-lang.org/)
- Web framework : [Jester](https://github.com/dom96/jester)

## Friends

- [FastDecoder](https://gitlab.com/xanhacks/fastdecoder/)