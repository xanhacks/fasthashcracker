import jester
from std/strutils import strip, isAlphaNumeric, toUpper

type Algorithm = enum MD5, SHA256, SHA512, ERROR

proc loadFileIntoArray(filename: string): seq[string] =
    var words: seq[string]

    for line in lines filename:
        words.add(line.strip())

    return words

const INDEX_HTML = readFile("index.html")
echo "Loading wordlists into memory ..."
let
    WORDLIST_PLAIN = loadFileIntoArray("rockyou.txt")
    WORDLIST_MD5 = loadFileIntoArray("rockyou.txt.md5")
    WORDLIST_SHA256 = loadFileIntoArray("rockyou.txt.sha256")
    WORDLIST_SHA512 = loadFileIntoArray("rockyou.txt.sha512")
echo "Done !"

proc checkHex(hash: string): bool =
    for c in hash:
        if isAlphaNumeric(c) != true:
            return false
    return true
    # return all(hash, proc (c: char): bool = isAlphaNumeric(c))

proc detectAlgorithm(hash: string): Algorithm =
    if checkHex(hash) == true:
        if hash.len == 32:
            return Algorithm.MD5
        elif hash.len == 64:
            return Algorithm.SHA256
        elif hash.len == 128:
            return Algorithm.SHA512

    return Algorithm.ERROR

proc searchStringInArray(wordlist: seq[string], hash: string): int =
    for index, word in wordlist.pairs:
        if word == hash:
            return index
    return -1

proc decrypt(hash: string, algo: Algorithm): string =
    var wordlist: seq[string]

    if algo == Algorithm.MD5:
        wordlist = WORDLIST_MD5
    elif algo == Algorithm.SHA256:
        wordlist = WORDLIST_SHA256
    elif algo == Algorithm.SHA512:
        wordlist = WORDLIST_SHA512

    let index = searchStringInArray(wordlist, hash)
    if index != -1 and index < WORDLIST_PLAIN.len:
        return WORDLIST_PLAIN[index]
    return "Error : Not found."


routes:
    get "/":
        resp INDEX_HTML
    get "/api/hash/@hash":
        let hash = toUpper(@"hash")
        let algo = detectAlgorithm(hash)
        if algo == Algorithm.ERROR:
            resp "Error : Unknow algorithm."
        resp decrypt(hash, algo)
